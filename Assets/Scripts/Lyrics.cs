﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Lyrics : GameController {
	static public float timeSound;
	static public string textOfLyrics;
	static public int foundARNumber;
	static public int scoreResult;
	static public int highScore = 0;
	static public string cardName;

	static public void LyricsTime(){
		if (timeSound > 8.45f) {
			textOfLyrics = "If you’re happy and you know it, clap your hands. ..... ";
			//Debug.Log ("Time :"+timeSound);
		} 
		if (timeSound > 16f) {
			textOfLyrics = "If you’re happy and you know it, And you really want to show it";
			//Debug.Log ("Time :"+timeSound);
		}
		if (timeSound > 20f) {
			textOfLyrics = "If you’re happy and you know it, clap your hands. .....";
			//Debug.Log ("Time :"+timeSound);
		}
		if (timeSound > 25f) {
			textOfLyrics = "If you’re happy and you know it, stomp your feet, .....";
			//Debug.Log ("Time :"+timeSound);
		}
		if (timeSound > 33f) {
			textOfLyrics = "If you’re happy and you know it, And you really want to show it";
			//Debug.Log ("Time :"+timeSound);
		}
		if (timeSound > 38f) {
			textOfLyrics = "If you’re happy and you know it, stomp your feet, .....";
			//Debug.Log ("Time :"+timeSound);
		}
		if (timeSound > 42.3f) {
			textOfLyrics = "If your happy and you know it, pat your head. .....";
			//Debug.Log ("Time :"+timeSound);
		}
		if (timeSound > 51f) {
			textOfLyrics = "If you’re happy and you know it, And you really want to show it";
			//Debug.Log ("Time :"+timeSound);
		}
		if (timeSound > 55.4f) {
			textOfLyrics = "If your happy and you know it, pat your head. .....";
			//Debug.Log ("Time :"+timeSound);
		}
		if (timeSound > 59.5f) {
			textOfLyrics = "If your happy and you know it, turn around!, .....";
			//Debug.Log ("Time :"+timeSound);
		}
		if (timeSound > 68.2f) {
			textOfLyrics = "If you’re happy and you know it, And you really want to show it";
			//Debug.Log ("Time :"+timeSound);
		}
		if (timeSound > 72.2f) {
			textOfLyrics = "If your happy and you know it, turn around!,.....";
			//Debug.Log ("Time :"+timeSound);
		}
		if (timeSound > 76.5f) {
			textOfLyrics = "If your happy and you know it, say hello!, .....";
			//Debug.Log ("Time :"+timeSound);
		}
		if (timeSound > 85.4f) {
			textOfLyrics = "If you’re happy and you know it, And you really want to show it";
			//Debug.Log ("Time :"+timeSound);
		}
		if (timeSound > 89.5f) {
			textOfLyrics = "If your happy and you know it, say hello!, .....";
			//Debug.Log ("Time :"+timeSound);
		}
		if (timeSound > 94f) {
			textOfLyrics = "End!";
			//playJumpBool = true;
		}
		if (timeSound > 100f){
			GameController.Save ();
			SceneManager.LoadScene ("ARSongResult");
			Debug.Log ("*****End Sound :"+timeSound);
			Debug.Log ("*****HighScore :"+highScore);
			Debug.Log ("*****ScoreResult :"+scoreResult);
		}

	}
	static public void SetscoreCardClap(){
		if (cardName == "CardClap") {
			PlayerController.checkAR = 1;
			PlayerController.score += 1;
			
		} else if (cardName == "CardStomp") {
			PlayerController.checkAR = 2;
			PlayerController.score += 0;
			
		} else if (cardName == "CardPat") {
			PlayerController.checkAR = 3;
			PlayerController.score += 0;
			
		} else if (cardName == "CardTurn") {
			PlayerController.checkAR = 4;
			PlayerController.score += 0;
			
		} else if (cardName == "CardSay") {
			PlayerController.checkAR = 5;
			PlayerController.score += 0;
			
		} else {
			PlayerController.checkAR = 0;
			PlayerController.score += 0;
			
		}
	}
	static public void SetscoreCardStomp(){
		if (cardName == "CardClap") {
			PlayerController.checkAR = 1;
			PlayerController.score += 0;
			
		} else if (cardName == "CardStomp") {
			PlayerController.checkAR = 2;
			PlayerController.score += 1;
			
		} else if (cardName == "CardPat") {
			PlayerController.checkAR = 3;
			PlayerController.score += 0;

			
		} else if (cardName == "CardTurn") {
			PlayerController.checkAR = 4;
			PlayerController.score += 0;
			
		} else if (cardName == "CardSay") {
			PlayerController.checkAR = 5;
			PlayerController.score += 0;
			
		} else {
			PlayerController.checkAR = 0;
			PlayerController.score += 0;
			
		}
	}
	static public void SetscoreCardPat(){
		if (cardName == "CardClap") {
			PlayerController.checkAR = 1;
			PlayerController.score += 0;
			
		} else if (cardName == "CardStomp") {
			PlayerController.checkAR = 2;
			PlayerController.score += 0;
			
		} else if (cardName == "CardPat") {
			PlayerController.checkAR = 3;
			PlayerController.score += 1;

			
		} else if (cardName == "CardTurn") {
			PlayerController.checkAR = 4;
			PlayerController.score += 0;
			
		} else if (cardName == "CardSay") {
			PlayerController.checkAR = 5;
			PlayerController.score += 0;
			
		} else {
			PlayerController.checkAR = 0;
			PlayerController.score += 0;
			
		}
	}
	static public void SetscoreCardTurn(){
		if (cardName == "CardClap") {
			PlayerController.checkAR = 1;
			PlayerController.score += 0;
			
		} else if (cardName == "CardStomp") {
			PlayerController.checkAR = 2;
			PlayerController.score += 0;
			
		} else if (cardName == "CardPat") {
			PlayerController.checkAR = 3;
			PlayerController.score += 0;

			
		} else if (cardName == "CardTurn") {
			PlayerController.checkAR = 4;
			PlayerController.score += 1;
			
		} else if (cardName == "CardSay") {
			PlayerController.checkAR = 5;
			PlayerController.score += 0;
			
		} else {
			PlayerController.checkAR = 0;
			PlayerController.score += 0;
			
		}
	}
	static public void SetscoreCardHello(){
		if (cardName == "CardClap") {
			PlayerController.checkAR = 1;
			PlayerController.score += 0;
			
		} else if (cardName == "CardStomp") {
			PlayerController.checkAR = 2;
			PlayerController.score += 0;
			
		} else if (cardName == "CardPat") {
			PlayerController.checkAR = 3;
			PlayerController.score += 0;

			
		} else if (cardName == "CardTurn") {
			PlayerController.checkAR = 4;
			PlayerController.score += 0;
			
		} else if (cardName == "CardSay") {
			PlayerController.checkAR = 5;
			PlayerController.score += 1;
			
		} else {
			PlayerController.checkAR = 0;
			PlayerController.score += 0;
			
		}
	}
}

