﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultController : GameController {


	public Text scorResultText;
	public Text hightScoreText;
	Animator animStar;
	GameObject StarGameObject;

	void Start () {
		Load ();
		StarGameObject = (GameObject.Find("Star"));
		animStar = StarGameObject.GetComponent<Animator>();
		animStar.SetBool("starPlay", false);
		scorResultText.text = "" + Lyrics.scoreResult;
		SetHighScore ();
		Lyrics.textOfLyrics = "";

		Debug.Log ("*****HighScore :"+Lyrics.highScore);
		Debug.Log ("*****ScoreResult :"+Lyrics.scoreResult);
	}
	public void SetHighScore(){
		if (Lyrics.scoreResult > Lyrics.highScore) {
			Lyrics.highScore = Lyrics.scoreResult;
			hightScoreText.text = "" + Lyrics.highScore;
			animStar.SetBool ("starPlay", true);
			Save ();
		} else if (Lyrics.scoreResult == Lyrics.highScore) {
			hightScoreText.text = "" + Lyrics.highScore;
			animStar.SetBool ("starPlay", true);
		}else {
			animStar.SetBool("starPlay", false);
			hightScoreText.text = "" + Lyrics.highScore;

		}


	}
	public void deleteHighScore(){
		PlayerPrefs.DeleteKey ("HighScore");
		hightScoreText.text = "0";
	}
}
