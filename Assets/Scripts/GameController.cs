﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;

public class GameController : MonoBehaviour {

	// Use this for initialization
	static public void Save(){
		PlayerPrefs.SetInt ("ScoreResult",Lyrics.scoreResult);
		PlayerPrefs.SetInt ("HighScore",Lyrics.highScore);
		PlayerPrefs.Save();
	}
	static public void Load(){
		Lyrics.scoreResult = PlayerPrefs.GetInt ("ScoreResult", 0);
		Lyrics.highScore = PlayerPrefs.GetInt ("HighScore", 0);
	}
	public void deleteScoreResult(){
		PlayerPrefs.DeleteKey ("ScoreResult");

		Lyrics.scoreResult = 0;
	}

	public void replay(){
		PlayerPrefs.DeleteKey ("ScoreResult");
		Lyrics.scoreResult = 0;
	}
}
