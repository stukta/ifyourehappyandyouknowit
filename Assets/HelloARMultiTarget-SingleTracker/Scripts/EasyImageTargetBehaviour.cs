//=============================================================================================================================
//
// Copyright (c) 2015-2017 VisionStar Information Technology (Shanghai) Co., Ltd. All Rights Reserved.
// EasyAR is the registered trademark or trademark of VisionStar Information Technology (Shanghai) Co., Ltd in China
// and other countries for the augmented reality technology developed by VisionStar Information Technology (Shanghai) Co., Ltd.
//
//=============================================================================================================================

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace EasyAR
{
    public class EasyImageTargetBehaviour : ImageTargetBehaviour
    {
		public AudioSource audio1;
		public string nameSoundAction;
        protected override void Awake()
        {
            base.Awake();
//			audio1 = gameObject.AddComponent<AudioSource>();

            TargetFound += OnTargetFound;
            TargetLost += OnTargetLost;
            TargetLoad += OnTargetLoad;
            TargetUnload += OnTargetUnload;
        }

        void OnTargetFound(TargetAbstractBehaviour behaviour)
        {
            Debug.Log("Found: " + Target.Id);
			Debug.Log("Found: " + Target.Name);

			Lyrics.cardName = Target.Name;

			Debug.Log ("timeSound"+Lyrics.timeSound);
			if (Lyrics.timeSound <= 12.29f && Lyrics.timeSound >= 10.6f) {
				audio1.Play ();
				Lyrics.SetscoreCardClap ();
				if(PlayerController.score >=1){
					PlayerController.score = 1;
				}
				PlayerController.soundPlaying = true;
			}
			if (Lyrics.timeSound <= 16.6f && Lyrics.timeSound >= 15.2f) {
				audio1.Play ();
				Lyrics.SetscoreCardClap ();
				if(PlayerController.score >=2){
					PlayerController.score = 2;
				}
				PlayerController.soundPlaying = true;
			}
			if (Lyrics.timeSound <= 25.2f && Lyrics.timeSound >= 23.5f) {
				this.audio1.Play ();
				Lyrics.SetscoreCardClap ();
				if(PlayerController.score >=3){
					PlayerController.score = 3;
				}
			}
			if (Lyrics.timeSound <= 29.5f && Lyrics.timeSound >= 28f) {
				Lyrics.SetscoreCardStomp ();
				audio1.Play();
				if(PlayerController.score >=4){
					PlayerController.score = 4;
				}
			}
			if (Lyrics.timeSound <= 33.8f && Lyrics.timeSound >= 32.3f) {
				Lyrics.SetscoreCardStomp ();
				audio1.Play();
				if(PlayerController.score >=5){
					PlayerController.score = 5;
				}
			}
			if (Lyrics.timeSound <= 42.4f && Lyrics.timeSound >= 40.9f) {
				Lyrics.SetscoreCardStomp ();
				audio1.Play();
				if(PlayerController.score >=6){
					PlayerController.score = 6;
				}
			}
			if (Lyrics.timeSound <= 46.8f && Lyrics.timeSound >= 45.3f) {
				Lyrics.SetscoreCardPat ();
				audio1.Play();
				if(PlayerController.score >=7){
					PlayerController.score = 7;
				}
			}
			if (Lyrics.timeSound <= 51.1f && Lyrics.timeSound >= 49.6f) {
				Lyrics.SetscoreCardPat ();
				audio1.Play();
				if(PlayerController.score >=8){
					PlayerController.score = 8;
				}
			}
			if (Lyrics.timeSound <= 59.7f && Lyrics.timeSound >= 58.2f) {
				Lyrics.SetscoreCardPat ();
				audio1.Play();
				if(PlayerController.score >=9){
					PlayerController.score = 9;
				}
			}
			if (Lyrics.timeSound <= 64f && Lyrics.timeSound >= 62.5f) {
				Lyrics.SetscoreCardTurn ();
				audio1.Play();
				if(PlayerController.score >=10){
					PlayerController.score = 10;
				}
			}
			if (Lyrics.timeSound <= 68.3f && Lyrics.timeSound >= 66.9f) {
				Lyrics.SetscoreCardTurn ();
				if(PlayerController.score >=11){
					PlayerController.score = 11;
				}
			}
			if (Lyrics.timeSound <= 76.9f && Lyrics.timeSound >= 75.6f) {
				Lyrics.SetscoreCardTurn ();
				audio1.Play();
				if(PlayerController.score >=12){
					PlayerController.score = 12;
				}
			}
			if (Lyrics.timeSound <= 81f && Lyrics.timeSound >= 79.8f) {
				Lyrics.SetscoreCardHello ();
				audio1.Play();
				if(PlayerController.score >=13){
					PlayerController.score = 13;
				}
			}
			if (Lyrics.timeSound <= 85.4f && Lyrics.timeSound >= 84f) {
				Lyrics.SetscoreCardHello ();
				audio1.Play();
				if(PlayerController.score >=14){
					PlayerController.score = 14;
				}
			}
			if (Lyrics.timeSound <= 93.9f && Lyrics.timeSound >= 92.6f) {
				Lyrics.SetscoreCardHello ();
				audio1.Play();
				if(PlayerController.score >=15){
					PlayerController.score = 15;
				}
			} else {
				if (Lyrics.cardName  == "CardClap") {
					PlayerController.checkAR = 1;
				} else if (Lyrics.cardName == "CardStomp") {
					PlayerController.checkAR = 2;
				} else if (Lyrics.cardName == "CardPat") {
					PlayerController.checkAR = 3;
				} else if (Lyrics.cardName == "CardTurn") {
					PlayerController.checkAR = 4;
				} else if (Lyrics.cardName == "CardSay") {
					PlayerController.checkAR = 5;
				} else {
					PlayerController.checkAR = 0;
				}
			}



        }

        void OnTargetLost(TargetAbstractBehaviour behaviour)
        {
            Debug.Log("Lost: " + Target.Id);
			PlayerController.checkAR = 0;
        }

        void OnTargetLoad(ImageTargetBaseBehaviour behaviour, ImageTrackerBaseBehaviour tracker, bool status)
        {
           // Debug.Log("Load target (" + status + "): " + Target.Id + " (" + Target.Name + ") " + " -> " + tracker);
        }

        void OnTargetUnload(ImageTargetBaseBehaviour behaviour, ImageTrackerBaseBehaviour tracker, bool status)
        {
           // Debug.Log("Unload target (" + status + "): " + Target.Id + " (" + Target.Name + ") " + " -> " + tracker);
        }
			
    }
}
