﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

	static public int checkAR = 0;
	public Text scoreText;
	static public int score = 0;
	// Use this for initialization
	public Animator animGirl;
	public Text LyricsText;
	GameObject GirlGameObject;
	AudioSource sourceName;
	static public bool soundPlaying = false;
	//static public bool foundAR;
	int GirlDanceIdle = Animator.StringToHash("Base Layer.GirlDanceIdle");
	int GirlDance1 = Animator.StringToHash("Base Layer.GirlDance0");
	int GirlDance2 = Animator.StringToHash("Base Layer.GirlDance1");
	int GirlDance3 = Animator.StringToHash("Base Layer.GirlDance2");
	int GirlDance4 = Animator.StringToHash("Base Layer.GirlDance3");
	int GirlDance5 = Animator.StringToHash("Base Layer.GirlDance4");
	
	void Start () {
		sourceName = gameObject.AddComponent<AudioSource>();
		sourceName.clip = Resources.Load("IfYouareHappyAndYouKnowIt") as AudioClip;
		sourceName.Play();


		GirlGameObject = (GameObject.Find("Cartoon"));
		animGirl = GirlGameObject.GetComponent<Animator>();
		score = 0;




	}
	
	// Update is called once per frame
	void Update () {
		Lyrics.timeSound = sourceName.time;
		Lyrics.LyricsTime ();
		LyricsText.text = Lyrics.textOfLyrics;



		//soundSetPause ();


		if (checkAR == 1) {
			animGirl.Play (GirlDance1);
		} else if (checkAR == 2) {
			animGirl.Play (GirlDance2);
		}  else if (checkAR == 3) {
			animGirl.Play (GirlDance3);
		}  else if (checkAR == 4) {
			animGirl.Play (GirlDance4);
		}  else if (checkAR == 5) {
			animGirl.Play (GirlDance5);
		} else {
			animGirl.Play (GirlDanceIdle);
		}
		scoreText.text = "" + score;
		Lyrics.scoreResult = score ;
		PlayerPrefs.SetInt ("ScoreResult",Lyrics.scoreResult);
	}


//	public void soundSetPause(){
//		
//		if (sourceName.time <= 12.29f && sourceName.time >= 11.50f && sourceName.isPlaying) {
//			sourceName.Pause ();
//			Debug.Log ("Pause 12.29f :"+soundPlaying);
//		} else if (sourceName.time <= 16.6f && sourceName.time >= 15.2f && sourceName.isPlaying) {
//			sourceName.Pause ();
//			Debug.Log ("Pause 13.2f:"+soundPlaying);
//		}else{
//			sourceName.UnPause ();
//			Debug.Log ("Notwork:"+soundPlaying);
//
//		}
//	}

//	IEnumerator delaysound(){
//		
//		yield return new WaitForSeconds (0.5f);
//		sourceName.UnPause ();
//	}

}

